#!/bin/bash
#
# NAMESPACE Remover версия 1.0 (15.03.2021)
# Автор: Барсуков В.В.
# Лицензия GPLv2 или выше.
#
# Описание:
# Средство удаления "застрявших" NAMESPACE (находящихся в стадии Terminating)
#
# Использование:
# ns-remover.sh
#

# ------------------------------------------------------------------
# Версия и дата
VERNUM="1.0"
VERDATE="15.03.2021"
# ------------------------------------------------------------------
# Меняем локаль на ru_RU.UTF-8
export LC_ALL=ru_RU.UTF-8

# Выводим шапку
echo
echo "+-------------------------------------------------------+"
echo "| Средство удаления NAMESPACE. Версия $VERNUM от $VERDATE |"
echo "| NAMESPACE Remover (NR)                                |"
echo "| (C) Барсуков В.В.                                     |"
echo "+-------------------------------------------------------+"
echo "| Формат: ns-remover.sh NAMESPACE                       |"
echo "+-------------------------------------------------------+"

# Если число передаваемых параметров меньше 1
if [[ "$#" -lt "1" ]]; then
    echo "NR: Ошибка. Нет обязательного аргумента."
    echo "NR: Доступные для удаления окружения:"
    kubectl get ns | grep "Terminating"
    exit 1
fi

# Получаем аргумент
NAMESPACE=$1

# Проверка наличия Namespace. Если в выходе "NotFound" - ругаемся и выходим
STATUS=$(kubectl get ns $NAMESPACE | grep "NotFound")
if ! [[ -z "$STATUS" ]]; then
    echo "NR: Ошибка. Окружение $NAMESPACE не найдено."
    echo "NR: Доступные для удаления окружения:"
    kubectl get ns | grep "Terminating"
    exit 2
fi

# Проверка статуса Namespace (удаляем только окружения в статусе Terminating)
STATUS=$(kubectl get ns $NAMESPACE | grep "Terminating")
if [[ -z "$STATUS" ]]; then
    echo "NR: Ошибка. Окружение $NAMESPACE не подходит для удаления."
    echo "NR: Доступные для удаления окружения:"
    kubectl get ns | grep "Terminating"
    exit 3
fi

# Проверка наличия подов в статусе "Running" в удаляемом окружении
STATUS=$(kubectl get pods --namespace $NAMESPACE | grep "Running")
if ! [[ -z "$STATUS" ]]; then
    echo "NR: Ошибка. Ещё остались запущенные поды в окружении $NAMESPACE:"
    kubectl get pods --namespace $NAMESPACE | grep "Running"
    exit 4
fi

# Готовим tmp.json фйал
kubectl get ns $NAMESPACE -o json  > tmp.json
awk 'NR==FNR{if (/spec/) hit=NR; next} {print} FNR==hit{exit}' tmp.json tmp.json > tmp2.json
echo '        "finalizers": []' >> tmp2.json
echo '    }' >> tmp2.json
echo '}' >> tmp2.json

cat tmp2.json

# Запускаем отдельный kubectl proxy
kubectl proxy &

# Небольшая пауза
sleep 3

# Удаляем окружение (вариант 2)
echo "NR: Удаляем окружение $NAMESPACE:"
curl -k -H "Content-Type: application/json" -X PUT --data-binary @tmp2.json http://127.0.0.1:8001/api/v1/namespaces/$NAMESPACE/finalize

# Удаляем окружение (вариант 1)
#kubectl patch namespace $NAMESPACE -p '{"metadata":{"finalizers":[]}}' --type='merge' -n $NAMESPACE
#kubectl delete namespace $NAMESPACE --grace-period=0 --force

# Проверяем удалено ли окружение
echo "NR: Проверяем удалено ли окружение $NAMESPACE:"
kubectl get ns $NAMESPACE

# Очищаем за собой
kill -9 $(ps aux | grep "kubectl proxy" | grep -v "grep" | awk '{print $2}')
rm tmp.json
rm tmp2.json

# Выход
exit 0
